from gurobipy import Column
#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------
def check_dual_constraint(model, x, distances, edge_coefficients_dictionary):
    """check for violated dual constraints and returns a set of edges to be added"""
    edges_to_be_added = set()
    epsilon_3 = 1e-03
    # model means the CVRP gurobi model
    constrs = model.getConstrs()
    dual_values = model.pi
    # two elements in the entry of the dictionary key constrs[i].ConstrName: value dual_values[i]
    dual = {constrs[i].ConstrName: dual_values[i] for i in range(model.numConstrs)}

    # Construct the dual constraint lhs
    # note that x.keys() = E_prime
    for (i, j) in (distances.keys() - x.keys()):
        if i == 0:
            lhs = dual[f"y_{j}"]
            lhs += sum(edge_coefficients_dictionary[i, j][constr] * dual[constr]
                       for constr in edge_coefficients_dictionary[i, j]
                       )
        else:
            lhs = dual[f"y_{i}"] + dual[f"y_{j}"]
            lhs += sum(dual[constr]
                       for constr in edge_coefficients_dictionary[i, j]
                       )
        # Check for violation
        if lhs >= distances[i, j] + epsilon_3:
            edges_to_be_added.add((i, j))
    return edges_to_be_added
#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------
def add_variables(model, x, distances, new_edges, edge_coefficients_dictionary):
    """Add variables to the model given a set of new edges"""
    # dictionary of constr. name and constr object
    constrsname = {constr.ConstrName: constr for constr in model.getConstrs()}
    for (i, j) in new_edges:
        if i != 0:
            coeffs = [1, 1]
            constrs = [constrsname[f"y_{i}"], constrsname[f"y_{j}"]]
        else:
            coeffs = [1]
            constrs = [constrsname[f"y_{j}"]]
        coeffs += [edge_coefficients_dictionary[i, j][constr] for constr in edge_coefficients_dictionary[i, j]]
        constrs += [constrsname[constr] for constr in edge_coefficients_dictionary[i, j]]
        # which coefficient and which constraint the variable (i, j) should be in
        col = Column(coeffs, constrs)

        if i == 0:
            x[i, j] = model.addVar(
                name=f"x_[{i},{j}]",
                obj=distances[i, j], column=col # this updates the constraints
            )
        else:
            x[i, j] = model.addVar(
                ub=1, name=f"x_[{i},{j}]",
                obj=distances[i, j], column=col
            )
#-----------------------------------------------------------------------------------------------------------------------
def Identify_Add_new_Edges(model, x, distances, E_prime, edge_coefficients_dictionary):
    """Add variables to a model and updates corresponding constraints"""
    added_vars = False

    # Find new variables
    new_vars = check_dual_constraint(model, x, distances, edge_coefficients_dictionary)

    if len(new_vars) > 0:
        added_vars = True
        # Add variables to model
        add_variables(model, x, distances, new_vars, edge_coefficients_dictionary)

    for edge in new_vars:
        edge_coefficients_dictionary.pop(edge) # remove added edges
    model.optimize()

    return added_vars, E_prime, edge_coefficients_dictionary
#-----------------------------------------------------------------------------------------------------------------------