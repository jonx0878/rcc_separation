import math
from os.path import exists
#----------------------------------------------------------------------------------------------------------
def load_CVRP_instance(filename):
    """Loads CVRP instance file"""
    if exists('Instances/' + filename + '.vrp'):
        filename = 'Instances/' + filename + '.vrp'
    else:
        filename = 'Instances/' + filename + '.txt'
    with open(filename) as f:
        # Load dimension and capacity
        dim_cap_type = 0
        while True:
            line = f.readline()
            if line.startswith("DIMENSION"):
                # instances from the CVRP lib imply parameter n to include the depot node
                # in our notations, n denotes the number of customers only, hence we correct this parameter
                n = [int(s) for s in line.split() if s.isdigit()][0] - 1
                dim_cap_type += 1

            elif line.startswith("CAPACITY"):
                Q = [int(s) for s in line.split() if s.isdigit()][0]
                dim_cap_type += 1

            elif line.startswith("EDGE_WEIGHT_TYPE"):
                edge_weight_type = line.split()[2]
                dim_cap_type += 1

            if dim_cap_type == 3:
                break
	
        if edge_weight_type == "EXPLICIT":
            # Load distance matrix
            while not line.startswith("EDGE_WEIGHT_SECTION"):
                line = f.readline()

            distances = {}
            for i in range(1, n + 1):
                line = f.readline()
                node_dists = [int(s) for s in line.split() if s.isdigit()]
                for j in range(len(node_dists)):
                    distances[j, i] = node_dists[j]

        # Load coordinates
        while not line.startswith("NODE_COORD_SECTION"):
            line = f.readline()

        coordinates = {}
        for i in range(n + 1):
            line = f.readline()
            coordinates[i] = [float(s) for s in line.split()][1:]

        # Load demand
        while not line.startswith("DEMAND_SECTION"):
            line = f.readline()
        demand = {}

        for i in range(n + 1):
            line = f.readline()
            demand[i] = [int(s) for s in line.split()][1]

    if edge_weight_type == "EUC_2D":
        print("Calculating distances")
        # Calculate Euclidean distances
        distances = {}
        for i in range(n + 1):
            for j in range(i + 1, n + 1):
                distances[i, j] = int(math.hypot(coordinates[i][0] - coordinates[j][0], coordinates[i][1] - coordinates[j][1]) + 0.5)
    return n, Q, coordinates, demand, distances
#----------------------------------------------------------------------------------------------------------
def load_CVRP_best_tour(filename):
    """Loads the best known solution"""
    with open('Instances/' + filename + ".sol") as f:
        line = f.readline()
        tours = {}
        i = 0
        while not line.startswith("Cost"):
            tours[i] = list(line.split())[2:]
            for j in range(len(tours[i])):
                tours[i][j] = int(tours[i][j])
            i += 1
            line = f.readline()

        upper_bound = float(list(line.split())[1])
    return tours, upper_bound
#----------------------------------------------------------------------------------------------------------