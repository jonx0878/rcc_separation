from gurobipy import *
import numpy as np
from itertools import product
from time import *
#-----------------------------------------------------------------------------------------------------------------------
def create_RC_cut(model, S):
    fraction = model._n * (model._n + 1) / (2 * model._n - 2)
    target_no_vehicles = np.ceil(sum(model._demand[i] for i in S) / model._Q)
    S_size = len(S)
    if S_size <= fraction:
        Constr_LHS = quicksum(model._y[i, j] for (i, j) in product(S, S) if i < j)
        Constr_RHS = S_size - target_no_vehicles
    else:
        S_bar = set(range(1, model._n + 1)).difference(S)
        Constr_LHS = quicksum(model._y[i, j] for (i, j) in product(S_bar, S_bar) if i < j) + \
                     0.5 * quicksum(model._y[0, i] for i in S_bar) - \
                     0.5 * quicksum(model._y[0, i] for i in S)
        Constr_RHS = len(S_bar) - target_no_vehicles
    return Constr_LHS, Constr_RHS
#-----------------------------------------------------------------------------------------------------------------------
def separate_Rounded_Capacity_cuts(gurobi_model, n, Q, demand, opt_obj, verbose):
    epsilon_1 = 0.5
    epsilon_2 = pow(10, -4)
    epsilon_3 = pow(10, -3)
    global_set_of_imposed_RC_cuts = set()
    fraction = n * (n + 1) / (2 * n - 2)
    def impose_RC_cut(gurobi_model, S, demand, Q):
        min_number_vehicles_for_S = np.ceil(np.sum(demand[k] for k in S) / Q)
        size_S = len(S)
        if size_S <= fraction:
            Constr_LHS = quicksum(gurobi_model._x[i, j] for (i, j) in product(S, S) if i < j)
            Constr_RHS = size_S - min_number_vehicles_for_S
        else:
            S_bar = set(range(1, n + 1)).difference(S)
            size_S_bar = n - size_S
            Constr_LHS = quicksum(gurobi_model._x[i, j] for (i, j) in product(S_bar, S_bar) if i < j) + \
                         0.5 * quicksum(gurobi_model._x[0, i] for i in S_bar) - \
                         0.5 * quicksum(gurobi_model._x[0, i] for i in S)
            Constr_RHS = size_S_bar - min_number_vehicles_for_S
        #if Constr_LHS.getValue() >= Constr_RHS + epsilon:
        S_in_string_form = str(S)
        if S_in_string_form not in global_set_of_imposed_RC_cuts:
            gurobi_model.addLConstr(Constr_LHS, GRB.LESS_EQUAL, Constr_RHS)
            global_set_of_imposed_RC_cuts.add(S_in_string_form)
            #f.write(S_in_string_form[1:len(S_in_string_form) - 1].replace(',', '') + '\n')

    def mycallback(model, where):
        if where == GRB.Callback.MIPSOL:
            if GRB.Callback.MIPSOL_OBJBST >= epsilon_3:
                S = {i for i in range(1, n + 1) if model.cbGetSolution(delta[i]) > 0.5}
                impose_RC_cut(gurobi_model, S, demand, Q)

    iteration_counter = 0
    delta = {}
    gamma = {}
    if verbose:
        start_time = time()
    #f = open(folder+'/'+filename+'_cuts.txt', 'w+')
    use_callback = True
    use_multiple_optimal_sol = False
    need_to_run_RCC_separation = True
    while need_to_run_RCC_separation:
        x_values = gurobi_model.getAttr("X", gurobi_model._x)
        m_separation = Model("Optimize")
        m_separation.setParam('OutputFlag', False)
        alpha = m_separation.addVar(vtype=GRB.INTEGER)
        relevant_edges = set()
        edge_capacities = {}
        for i in range(1, n + 1):
            delta[i] = m_separation.addVar(vtype=GRB.BINARY)
            for j in range(i + 1, n + 1):
                edge_capacity = x_values[i, j]
                if edge_capacity >= epsilon_2:
                    relevant_edges.add((i, j))
                    edge_capacities[i, j] = edge_capacity
                    gamma[i, j] = m_separation.addVar(vtype=GRB.CONTINUOUS)
        m_separation.addLConstr(Q * alpha + epsilon_1, GRB.LESS_EQUAL, quicksum(delta[i] * demand[i] for i in range(1, n + 1)))
        obj_separation = LinExpr(2 * alpha + 2)
        for i in range(1, n + 1):
            edge_capacity = x_values[0, i]
            if edge_capacity >= epsilon_2:
                obj_separation.add(-delta[i] * edge_capacity)
        for (i, j) in relevant_edges:
            m_separation.addLConstr(gamma[i, j], GRB.LESS_EQUAL, delta[i])
            m_separation.addLConstr(gamma[i, j], GRB.LESS_EQUAL, delta[j])
        m_separation.addLConstr(quicksum(delta[i] for i in range(1, n + 1)), GRB.GREATER_EQUAL, 2)
        m_separation.setObjective(obj_separation - quicksum((delta[i] + delta[j] - 2 * gamma[i, j]) * edge_capacities[i, j] for (i, j) in relevant_edges), GRB.MAXIMIZE)

        CVRP_model_size = gurobi_model.NumConstrs
        if use_callback:
            m_separation.optimize(mycallback)
        else:
            m_separation.optimize()

        if m_separation.STATUS == 2:
            iteration_counter += 1
            if m_separation.objval >= epsilon_3:
                if use_multiple_optimal_sol:
                    for sol in range(m_separation.solcount):
                        m_separation.setParam('SolutionNumber', sol)
                        S = {i for i in range(1, n + 1) if delta[i].xn > 0.5}
                        impose_RC_cut(gurobi_model, S, demand, Q)
                else:
                    S = {i for i in range(1, n + 1) if delta[i].x > 0.5}
                    impose_RC_cut(gurobi_model, S, demand, Q)
                gurobi_model.optimize()
            if verbose:
                if iteration_counter % 100 == 0:
                    if opt_obj:
                        print(iteration_counter, ': ', round(time() - start_time, 1), ' sec. ', np.round(100 * gurobi_model.objval / opt_obj, 2),'%')
                    else:
                        print(iteration_counter, ': ', round(time() - start_time, 1), ' sec. ', np.round(gurobi_model.objval, 2))
            if gurobi_model.NumConstrs == CVRP_model_size:
                need_to_run_RCC_separation = False
    #f.close()
    return gurobi_model.objVal, len(global_set_of_imposed_RC_cuts), iteration_counter
#-----------------------------------------------------------------------------------------------------------------------