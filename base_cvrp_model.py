from gurobipy import Model, quicksum, GRB
#----------------------------------------------------------------------------------------------------------
def base_CVRP_model(filename, n, edges):
    """Set up the base CVRP model"""
    model = Model(filename)

    # Add variables
    x = model.addVars([(i, j) for (i, j) in edges if i > 0], ub=1, name="x_")
    x.update(model.addVars([(0, i) for i in range(1, n + 1) if (0, i) in edges], name="x_"))

    # Add constraints
    for i in range(1, n + 1):
        model.addLConstr(quicksum(x[j, i] for j in range(i) if (j, i) in x)
                         + quicksum(x[i, j] for j in range(i + 1, n + 1) if (i, j) in x),
                         GRB.EQUAL, 2, name=f"y_{i}")

    # Set the objective
    model.setObjective(quicksum(edges[key] * x[key] for key in edges), GRB.MINIMIZE)

    # Change the output parameter
    model.setParam('OutputFlag', False)
    # Set Dual Simplex as LP solver for the model
    model.setParam('Method', 1)

    return model, x
#----------------------------------------------------------------------------------------------------------