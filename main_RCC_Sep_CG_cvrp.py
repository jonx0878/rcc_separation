#-----------------------------------------------------------------------------------------------------------------------
import math

from timeit import default_timer as timer
import numpy as np

from load_cvrp_instance import load_CVRP_best_tour, load_CVRP_instance
from base_cvrp_model import base_CVRP_model
from initial_E_prime import start_edges_shortest, start_edges_tours
from utilities_Col_Gen import Identify_Add_new_Edges
from utilities_RCC_Sep_CG import run_RCC_separation
#-----------------------------------------------------------------------------------------------------------------------
def RCC_column_generation(filename, time_limit=None, use_callback=True, use_multiple_opt_solutions=False):
    # The main function for determining a lower bound of the CVRP using RC cuts via the column generation method"
    # Load instance - Note that distances includes set of all edges
    n, Q, coordinates, demand, distances = load_CVRP_instance(filename)
    # Define initial E_prime: m = 0 implies BKS only; m > 0 implies m shortest edges for every i included into initial E_prime
    m = 15

    # Determine the min number of vehicles
    num_depot_edges = math.ceil(sum(demand.values()) / Q)

    # Load best known solution and determine the initial E_prime set
    tours, upper_bound = load_CVRP_best_tour(filename)
    if m == 0:
        E_prime = start_edges_tours(distances, tours)
    else:
        """Consider including the BKS """
        E_prime = start_edges_shortest(
            n, distances, int(m), tours, num_depot_edges
        )

    # Setup models
    start_time = timer()
    model, model_vars = base_CVRP_model(filename, n, E_prime)

    # edge_coefficients_dictionary is a dictionary that for every edge {i, j} in E \ E_prime and
    # every RC cut (identified by set S) returns the coefficient in front of the edge {i, j} in E \ E_prime
    # in the appropriate form of the RC cut.
    # at first initialized an an empty dictionary of edges in E \ E_prime
    edge_coefficients_dictionary = {edge: {} for edge in distances.keys() - E_prime.keys()}
    model.optimize()
    # Solve model
    print('Initial relaxation -',
          'Obj:', model.objval,
          'Vars:', model.numVars
         )
    continue_separation = True
    iteration = 0
    while continue_separation:
        iteration += 1
        # Add variables
        continue_separation, E_prime, edge_coefficients_dictionary = Identify_Add_new_Edges(
            model, model_vars,
            distances, E_prime,
            edge_coefficients_dictionary
        )
        if iteration == 1:
            continue_separation = True
        elif not continue_separation:
            break

        model.optimize()

        if time_limit:
            if timer() - start_time > time_limit:
                time_limit_reached = True
                break
        # Add RC cuts
        time_limit_reached = run_RCC_separation(
            model, model_vars,
            demand, Q, n,
            edge_coefficients_dictionary, start_time, time_limit,
            use_callback, use_multiple_opt_solutions
        )
        if time_limit_reached:
            break
        # Print iteration results
        print('Iteration:', iteration, 'Time:', np.round(timer() - start_time, 1), 'Obj:', model.objval, 'Vars:', model.numvars, 'Total Cuts:', model.numConstrs)

    if time_limit_reached:
        print('Time limit was reached. Determining lower bound')
        vars_added = True
        while vars_added:
            vars_added, E_prime, edge_coefficients_dictionary = Identify_Add_new_Edges(
                model, model_vars,
                distances, E_prime,
                edge_coefficients_dictionary
            )

    finish_time = timer()
    # Print results
    print('Instance: ', filename)
    print('Time:', np.round(finish_time - start_time, 1),
          'Obj:',  np.round(model.objval, 3), np.round(model.objval / upper_bound * 100, 2),
          'Vars:', model.numvars,
          'Total Cuts:', model.numConstrs)
    print()
#-----------------------------------------------------------------------------------------------------------------------
def X_instances():
    RCC_column_generation('X-n242-k48')
    RCC_column_generation('X-n261-k13')
    RCC_column_generation('X-n280-k17')
    RCC_column_generation('X-n308-k13')
    RCC_column_generation('X-n331-k15')
    RCC_column_generation('X-n367-k17')
    RCC_column_generation('X-n411-k19')
    RCC_column_generation('X-n459-k26')
    RCC_column_generation('X-n513-k21')
#----------------------------------------------------------------------------------------------------------------
def DIMACS_instances():
    time_lim = 72000
    RCC_column_generation('ORTEC-n242-k12', time_limit=time_lim)
    RCC_column_generation('ORTEC-n323-k21', time_limit=time_lim)
    RCC_column_generation('ORTEC-n405-k18', time_limit=time_lim)
    RCC_column_generation('ORTEC-n455-k41', time_limit=time_lim)
    RCC_column_generation('ORTEC-n510-k23', time_limit=time_lim)
    RCC_column_generation('ORTEC-n701-k64', time_limit=time_lim)
    RCC_column_generation('Loggi-n401-k23', time_limit=time_lim)
    RCC_column_generation('Loggi-n501-k24', time_limit=time_lim)
    RCC_column_generation('Loggi-n601-k19', time_limit=time_lim)
    RCC_column_generation('Loggi-n601-k42', time_limit=time_lim)
    RCC_column_generation('Loggi-n901-k42', time_limit=time_lim)
    RCC_column_generation('Loggi-n1001-k31', time_limit=time_lim)
#----------------------------------------------------------------------------------------------------------------
def Ultra_Large():
    time_lim = 3 * 3600
    RCC_column_generation("Leuven1", time_limit=time_lim)
    RCC_column_generation("Leuven2", time_limit=time_lim)
    RCC_column_generation("Antwerp1", time_limit=time_lim)
    RCC_column_generation("Antwerp2", time_limit=time_lim)
    time_lim = 5 * 3600
    RCC_column_generation("Leuven1", time_limit=time_lim)
    RCC_column_generation("Leuven2", time_limit=time_lim)
    RCC_column_generation("Antwerp1", time_limit=time_lim)
    RCC_column_generation("Antwerp2", time_limit=time_lim)
#----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    X_instances()
    #DIMACS_instances()
    #Ultra_Large()