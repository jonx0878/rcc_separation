import math
from gurobipy import Model, quicksum, GRB
from timeit import default_timer as timer
#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------
def add_RC_constraint(n, model, x, S, demand, Q, solution, epsilon_3, edge_coefficients_dictionary):
    """Adds a single RC cut"""
    min_number_of_vehicles_S = math.ceil(sum(demand[k] for k in S) / Q)
    Constr_edges = {(i, j): 1 for i in S for j in S if i < j}
    Constr_edges_in_model = {(i, j): 1 for (i, j) in set(Constr_edges.keys()) & set(x.keys())}
    number_of_variables_in_S = len(Constr_edges_in_model)
    number_of_variables_in_S_bar = len(x.keys()) - number_of_variables_in_S

    if number_of_variables_in_S <= number_of_variables_in_S_bar:
        Constr_LHS = quicksum(x[i, j] for (i, j) in Constr_edges_in_model)
        Constr_LHS_val = sum(solution[i, j] for (i, j) in Constr_edges_in_model)
        Constr_RHS = len(S) - min_number_of_vehicles_S
    else:
        S_bar = set(range(1, n + 1)).difference(S)
        Constr_edges = {(i, j): 1 for i in S_bar for j in S_bar if i < j}
        Constr_edges.update({(0, i): 0.5 for i in S_bar})
        Constr_edges.update({(0, i): -0.5 for i in S})
        Constr_edges_in_model = {(i, j): Constr_edges[i, j] for (i, j) in (set(Constr_edges.keys()) & set(x.keys()))}

        Constr_LHS = quicksum(Constr_edges_in_model[i, j] * x[i, j] for (i, j) in Constr_edges_in_model)
        Constr_LHS_val = sum(Constr_edges_in_model[i, j] * solution[i, j] for (i, j) in Constr_edges_in_model)
        Constr_RHS = len(S_bar) - min_number_of_vehicles_S
    if Constr_LHS_val >= Constr_RHS + epsilon_3:
        Constr_edges_not_in_model = {(i, j): Constr_edges[i, j]
                                     for (i, j) in (set(Constr_edges.keys()) - set(Constr_edges_in_model.keys()))}
        model.update()
        cut_number = model.NumConstrs - (n + 1) + 1
        model.addLConstr(Constr_LHS, GRB.LESS_EQUAL, Constr_RHS, name=f"cut_{cut_number}")

        cut_name = f"cut_{cut_number}"
        for edge in Constr_edges_not_in_model:
            edge_coefficients_dictionary[edge][cut_name] = Constr_edges_not_in_model[edge]
    return edge_coefficients_dictionary
#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------
def create_RCC_separation_model(n, x, Q, demand, epsilon):
    """Create the RCC separation model"""

    m = Model("Optimize")
    m.setParam('OutputFlag', False)
    delta = {}
    gamma = {}
    epsilon_1 = 0.5

    # Add variables
    alpha = m.addVar(vtype=GRB.INTEGER, name="alpha")
    for i in range(1, n + 1):
        delta[i] = m.addVar(vtype=GRB.BINARY, name=f"delta_{i}")

    for (i, j) in x:
        if i != 0 and x[i, j].x >= epsilon:
            gamma[i, j] = m.addVar(name=f"gamma_{(i, j)}")
            m.addLConstr(gamma[i, j], GRB.LESS_EQUAL, delta[i])
            m.addLConstr(gamma[i, j], GRB.LESS_EQUAL, delta[j])
            
    # Add constraints
    m.addLConstr(Q * alpha + epsilon_1, GRB.LESS_EQUAL, quicksum(delta[i] * demand[i] for i in range(1, n + 1)))
    m.addLConstr(quicksum(delta[i] for i in range(1, n + 1)), GRB.GREATER_EQUAL, 2)

    # Set objective
    obj = (2 * alpha + 2
           - quicksum(x[0, i].x * delta[i] for i in range(1, n + 1) if (0, i) in x)
           - quicksum(x[i, j].x * (delta[i] + delta[j] - 2 * gamma[i, j])
                      for (i, j) in gamma))

    m.setObjective(obj, GRB.MAXIMIZE)
    return m, delta
#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------
def run_RCC_separation(model, x, demand, Q, n, edge_coefficients_dictionary, start_time, time_limit, use_callback, use_multiple_opt_solutions):
    """Separation routine for RC cuts"""
    epsilon_2 = pow(10, -4)
    epsilon_3 = pow(10, -3)
    time_limit_reached = False

    # Define Callback function
    def myCallback(cb_model, where):
        if where == GRB.Callback.MIPSOL:
            sol = cb_model.cbGetSolution(delta)
            S = {i for i in range(1, n + 1) if sol[i] > 0.5}
            cb_model._coefficients_of_vars = add_RC_constraint(
                n, model, x, S, demand, Q, m_Separation._current_sol,
                epsilon_3, cb_model._coefficients_of_vars
            )

    # Run separation
    need_to_run_RCC_separation = True
    while need_to_run_RCC_separation:
        if time_limit:
            if timer() - start_time > time_limit:
                time_limit_reached = True
                break

        current_sol = {(i, j): x[i, j].x for (i, j) in x}
        m_Separation, delta = create_RCC_separation_model(n, x, Q, demand, epsilon_2)
        m_Separation._coefficients_of_vars = edge_coefficients_dictionary
        m_Separation._current_sol = current_sol
        CVRP_model_size = model.NumConstrs
        if use_callback:
            m_Separation.optimize(myCallback)
        else:
            m_Separation.optimize()
        edge_coefficients_dictionary = m_Separation._coefficients_of_vars

        # Obtain separation model solution(s) and add corresponding cuts
        if m_Separation.STATUS == 2:
            if use_multiple_opt_solutions:
                for sol in range(m_Separation.solcount):
                    m_Separation.setParam('SolutionNumber', sol)
                    S = {i for i in range(1, n + 1) if delta[i].xn > 0.5}
                    edge_coefficients_dictionary = add_RC_constraint(n, model, x, S, demand, Q, current_sol, epsilon_3, edge_coefficients_dictionary)
            else:
                S = {i for i in range(1, n + 1) if delta[i].x > 0.5}
                edge_coefficients_dictionary = add_RC_constraint(n, model, x, S, demand, Q, current_sol, epsilon_3, edge_coefficients_dictionary)

            model.update()
            if model.NumConstrs == CVRP_model_size:
                need_to_run_RCC_separation = False
            else:
                model.optimize()
                #print('CVRP obj: ', round(model.objval, 2), 'Iter. time: ', round(time() - iteration_start_time, 1), 'Sep. time: ', round(mm.runtime, 1), 'Sep. obj: ', np.round(mm.objval + 2, 1), 'Total time: ', round(time() - start_time, 1))
    return time_limit_reached
#-----------------------------------------------------------------------------------------------------------------------