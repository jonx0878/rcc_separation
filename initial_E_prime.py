from operator import itemgetter
#----------------------------------------------------------------------------------------------------------
def start_edges_tours(distances, tours):
    """Returns edges used in given tours"""
    edges = {}
    # Add variables from specified tours
    for tour in tours:
        for i in range(len(tours[tour]) - 1):
            if tours[tour][i] < tours[tour][i + 1]:
                edge = (tours[tour][i], tours[tour][i + 1])
            else:
                edge = (tours[tour][i + 1], tours[tour][i])
            edges[edge] = distances[edge]
        edges[0, tours[tour][0]] = distances[0, tours[tour][0]]
        edges[0, tours[tour][-1]] = distances[0, tours[tour][-1]]
    return edges
#----------------------------------------------------------------------------------------------------------
def start_edges_shortest(n, distances, m, tours, num_depot_edges):
    """Returns edges used in starting solutions -m shortest edges for each node"""
    edges = start_edges_tours(distances, tours)
    # Add num_depot_edges from the depot
    temp_dist = {(0, i): distances[0, i] for i in range(1, n + 1)}
    edges.update(dict(sorted(temp_dist.items(), key=itemgetter(1))[:num_depot_edges]))
    # Add m shortest edges for each node
    for i in range(1, n + 1):
        temp_dist = {(j, i): distances[j, i] for j in range(i)}
        temp_dist.update({(i, j): distances[i, j] for j in range(i + 1, n + 1)})
        edges.update(dict(sorted(temp_dist.items(), key=itemgetter(1))[:m]))
    return edges
#----------------------------------------------------------------------------------------------------------